# --- Special code for migrating to Rails 5.0 ---
def rails5?
  %w[1 true].include?(ENV["RAILS5"])
end

gem_versions = {}
gem_versions['activerecord_sane_schema_dumper'] = rails5? ? '1.0'      : '0.2'
gem_versions['default_value_for']               = rails5? ? '~> 3.0.5' : '~> 3.0'
gem_versions['rails']                           = rails5? ? '5.0.7'    : '4.2.10'
gem_versions['rails-i18n']                      = rails5? ? '~> 5.1'   : '~> 4.0.9'
# --- The end of special code for migrating to Rails 5.0 ---

source 'https://rubygems.org'
gem 'rails', gem_versions['rails']

# Use packaged native gems
gem 'rake', '12.3.1'
gem 'rails-dom-testing', '1.0.6'
gem 'nokogiri', '1.8.4'
gem 'pg', '~> 0.19.0'
gem 'rack', '~> 1.6.4'
gem 'sprockets-rails', '~> 2.3.2'

if ENV["INCLUDE_TEST_DEPENDS"] == "true"
  gem 'bootsnap', '~> 1.3'
  gem 'pry-byebug', '~> 3.4.1', platform: :mri
  gem 'pry-rails', '~> 0.3.4'

  gem 'awesome_print', require: false
  gem 'fuubar', '~> 2.2.0'

  gem 'database_cleaner', '~> 1.5.0'
  gem 'factory_bot_rails', '~> 4.8.2'
  gem 'rspec-rails', '~> 3.7' # Use packaged version
  gem 'rspec-retry', '~> 0.4.5'
  gem 'rspec_profiling', '~> 0.0.5'
  gem 'rspec-set', '~> 0.1.3'
  gem 'rspec-parameterized', require: false

  # Prevent occasions where minitest is not bundled in packaged versions of ruby (see #3826)
  gem 'minitest', '~> 5.7.0'

  # Generate Fake data
  gem 'ffaker', '~> 2.4'

  gem 'capybara', '~> 2.15'
  gem 'capybara-screenshot', '~> 1.0.0'
  gem 'selenium-webdriver', '~> 3.12'

  gem 'spring', '~> 2.0.0'
  gem 'spring-commands-rspec', '~> 1.0.4'

  gem 'gitlab-styles', '~> 2.4', require: false
  # Pin these dependencies, otherwise a new rule could break the CI pipelines
  gem 'rubocop', '~> 0.54.0'
  gem 'rubocop-rspec', '~> 1.22.1'

  gem 'scss_lint', '~> 0.56.0', require: false
  gem 'haml_lint', '~> 0.26.0', require: false
  gem 'simplecov', '~> 0.14.0', require: false
  gem 'bundler-audit', '~> 0.5.0', require: false

  gem 'benchmark-ips', '~> 2.3.0', require: false

  gem 'license_finder', '~> 5.4', require: false
  gem 'knapsack', '~> 1.16'

  gem 'activerecord_sane_schema_dumper', gem_versions['activerecord_sane_schema_dumper']

  gem 'stackprof', '~> 0.2.10', require: false

  gem 'simple_po_parser', '~> 1.1.2', require: false

  gem 'timecop', '~> 0.8.0'
#end

#group :test do
  gem 'shoulda-matchers', '~> 3.1.2', require: false
  gem 'email_spec', '~> 2.2.0'
  gem 'json-schema', '~> 2.8.0'
  gem 'webmock', '>= 2.3.2'
  gem 'rails-controller-testing' if rails5? # Rails5 only gem.
  gem 'test_after_commit', '~> 1.1' unless rails5? # Remove this gem when migrated to rails 5.0. It's been integrated to rails 5.0.
  gem 'sham_rack', '~> 1.3.6'
  gem 'concurrent-ruby', '~> 1.0.5'
  gem 'test-prof', '~> 0.2.5'
  gem 'rspec_junit_formatter'
end
